#include <sys/socket.h>
#include <unistd.h> 
#include <netinet/in.h> 
#include "message.h"
#include "user.h"

class Server{
    int port = 8080;
    int server_fd = 0;
    sockaddr_in address;
    int addrlen = sizeof(address);
    int opt;

    Messages msgs;
    Users users;
public:
    Server();
    void start();
    // RESPONSES //
    void SendChat(int);
    void AddMessage(int);
    void SendUser(int);
    void SendAllUsers(int);
};
