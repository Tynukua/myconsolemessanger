#include "message.h"

PositionMessage::PositionMessage(){
    Message::GetMessageType() = PositionMessageType;
    x = 0;y = 0;
}
void PositionMessage::display(std::ostream &os){
    os << '(' << x << ':' << y << ')';
    Message::display(os);
}

void PositionMessage::read(std::ifstream&is){
    Message::read(is);
    is.read((char *)&x, sizeof(x));
    is.read((char *)&y, sizeof(y));
}

void PositionMessage::read(std::istream&is){
    Message::read(is);
    is >> x >> y;
}

void PositionMessage::write(std::ofstream &os){
    Message::write(os);
    os.write((char *)&x, sizeof(x));
    os.write((char *)&y, sizeof(y));
}

void PositionMessage::read(ISocketFileStream&is){
    Message::read(is);
    is.read((char *)&x, sizeof(x));
    is.read((char *)&y, sizeof(y));
}

void PositionMessage::write(OSocketFileStream &os){
    Message::write(os);
    os.write((char *)&x, sizeof(x));
    os.write((char *)&y, sizeof(y));
}
