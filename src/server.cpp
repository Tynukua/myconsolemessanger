#include <string>
#include <ctime>

#include "request.h"
#include "server.h"
#include "socketfile.h"

Server::Server():msgs("messages.db"),users("users.db"){
    // Creating socket file descriptor 
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
    { 
        throw std::string("SocketFailed"); 
    } 
       
    // Forcefully attaching socket to the port 8080 
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, 
                                                  &opt, sizeof(opt))) 
    { 
        throw std::string("SetsockoptFailed"); 

    } 
    address.sin_family = AF_INET; 
    address.sin_addr.s_addr = INADDR_ANY; 
    address.sin_port = htons( this->port ); 
       
    // Forcefully attaching socket to the port 8080 
    if (bind(server_fd, (struct sockaddr *)&address,  
                                 sizeof(address))<0) 
    { 
        throw std::string("BindFailed"); 
    } 
    if (listen(server_fd, 3) < 0) 
    { 
        throw std::string("ListenFailed"); 
    } 
}
void Server::start(){
    int socketfd;
    while((socketfd = accept(server_fd, (struct sockaddr *)&address,  
                                       (socklen_t*)&addrlen))>=0){
       ISocketFileStream isocket(socketfd);
       RequestTypes requsettype;
       isocket.read((char*)&requsettype, sizeof(requsettype));
       auto timer = time(NULL);
       auto tm_info = localtime(&timer);
       char bufer[32] {0};
       strftime(bufer, 26, "%Y-%m-%d %H:%M:%S", tm_info);
       printf("[%s]", bufer);
       switch(requsettype){
            case GetChatRequest:

                SendChat(socketfd);
                puts("SendChat");
                break;
            case GetMyUserRequest:
                SendUser(socketfd);
                puts("SendUser");
                break;
            case GetUsersRequests:
                SendAllUsers(socketfd);
                puts("SendAllUser");
                break;
            case SendMessageRequest:
                AddMessage(socketfd);
                puts("SendMessage");
                break;
            default:
                break;
       }
       close(socketfd);
    }

}

void Server::AddMessage(int socketfd){
    ISocketFileStream isocket(socketfd);
    Message * msg;
    Message::read(isocket, msg);
    std::cout << users[msg->UserId()]->Username();
    std::cout << msg->UserId();
    msgs.add(msg);
}

void Server::SendUser(int sockfd){
    ISocketFileStream isocket(sockfd);
    size_t len = 0;
    isocket.read((char*)&len,sizeof(len));
    char * buf = new char[len+1]{0};
    isocket.read(buf, len);
    std::string name = buf;
    std::cout << name;
    bool finded = 0;
    size_t id = 0;
    for(size_t i = 0;i<users.Len();i++){
        if(users[i]->username==name){
            finded = 1;
            id = i;
            break;
        }
    }
    if(!finded){
        auto n = new User;
        n->username = name;
        users.add(n);
        id = users.Len()-1;
    }
    OSocketFileStream osocket(sockfd);
    osocket.write((char*)&id, sizeof(id));
}

void Server::SendChat(int sockfd){
    OSocketFileStream osocket(sockfd);
    msgs.save(osocket);
    
}

void Server::SendAllUsers(int sockfd){
    OSocketFileStream osocket(sockfd);
    users.save(osocket);

}

