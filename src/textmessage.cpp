#include "message.h"
#include <fstream>


TextMessage::TextMessage(){
    Message::GetMessageType() = TextMessageType;
    text = "";
}

void TextMessage::write(std::ofstream &os){
    Message::write(os);
    size_t t = text.size();
    os.write((char*) &t, sizeof(size_t));
    for (int i = 0;i<text.size();i++){
        os.put(text[i]);
    }

}
void TextMessage::write(OSocketFileStream &os){
    Message::write(os);
    size_t t = text.size();
    os.write((char*) &t, sizeof(size_t));
    for (int i = 0;i<text.size();i++){
        os.put(text[i]);
    }

}
void TextMessage::display(std::ostream &os){
    os<< text;
    Message::display(os);
}
void TextMessage::read(std::istream &is){
    Message::read(is);
    std::getline(std::cin, text);
}

void TextMessage::read(std::ifstream &is){
    Message::read(is);
    size_t t;
    text = "";
    is.read((char*)&t, sizeof(t));
    for(int i = 0; i <t; i++){
        text += is.get();
    }
}
void TextMessage::read(ISocketFileStream &is){
    Message::read(is);
    size_t t;
    text = "";
    is.read((char*)&t, sizeof(t));
    for(int i = 0; i <t; i++){
        text += is.get();
    }
}
