#ifndef MESSAGE_H
#define MESSAGE_H
#include <ctime>
#include <string>
#include <iostream>
#include "filecontainer.h"
#include "socketfile.h"

enum MessageTypes{
    MessageType,
    TextMessageType,
    ImageMessageType,
    PositionMessageType    
};

class Message{
    time_t _time;
    size_t user_id = 0;
    MessageTypes msg_type = MessageType;
public:
    MessageTypes& GetMessageType();
    time_t Time();
    size_t& UserId();
    Message();
    static void read(ISocketFileStream &is, Message *&msg);
    virtual void write(OSocketFileStream&os);
    virtual void read(ISocketFileStream&is);
    static void read(std::ifstream& is, Message*&msg);
    virtual void write(std::ofstream& os);
    virtual void read(std::ifstream&is);
    virtual void display(std::ostream& os);
    virtual void read(std::istream&is);
};

class TextMessage:public Message{
    std::string text = "";
public:
    TextMessage();
    virtual void write(OSocketFileStream&os);
    virtual void read(ISocketFileStream&is);
    virtual void  write(std::ofstream& os);
    virtual void  read(std::ifstream&is);
    virtual void display(std::ostream& os);
    virtual void read(std::istream&is);

};

class ImageMessage: public Message{
    char image[20][20];
public:
    ImageMessage(char f = ' ');
    virtual void write(OSocketFileStream&os);
    virtual void read(ISocketFileStream&is);
    virtual void write(std::ofstream& os);
    virtual void read(std::ifstream&is);
    virtual void display(std::ostream& os);
    virtual void read(std::istream&is);

};

class PositionMessage:public Message{
    float x = 0;
    float y = 0;
public:
    PositionMessage();
    virtual void write(OSocketFileStream&os);
    virtual void read(ISocketFileStream&is);
    virtual void write(std::ofstream& os);
    virtual void read(std::ifstream&is);
    virtual void display(std::ostream& os);
    virtual void read(std::istream&is);
};

typedef FileContainer<Message> Messages ;
#endif
