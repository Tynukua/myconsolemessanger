#include "socketfile.h"
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h> 

OSocketFileStream::OSocketFileStream(int fd){
    socketfd = fd;
}

ISocketFileStream::ISocketFileStream(int fd){
    socketfd = fd;
}

// Write into socket

void OSocketFileStream::write(const char *s, std::streamsize n){
    send(socketfd, s, n, 0);
}
void  OSocketFileStream::put (char c){
    send(socketfd,&c, sizeof(c) ,0);

}


// Read from soket

void ISocketFileStream::read(char *s, std::streamsize n){
    auto bytes = ::read(socketfd, s, n);
}


int ISocketFileStream::get(){
    char c;
    auto bytes = ::read(socketfd, &c, sizeof(c));
    return c;
}
