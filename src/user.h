#ifndef USER_H
#define USER_H

#include <string>
#include "filecontainer.h"

class User{
    friend class Server;
    std::string username = "User";
    public:
    std::string Username();
    void read(ISocketFileStream& is);
    static void read(ISocketFileStream& is, User*&userptr);
    static void read(std::ifstream& is, User*&userptr);
    void write(std::ofstream&);
    void write(OSocketFileStream&);
    void read(std::ifstream&);
    
};
typedef FileContainer<User> Users;
#endif
