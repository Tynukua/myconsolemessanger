#include "client.h"

int main(int argc, char * argv[]){
    try{
        char * name = "Guest";
        if (argc>1)
            name = argv[1];
        char ip[] = "127.0.0.1";
        Client client(ip, name);
        client.GetMyUser();
        
        while(1){
            system("clear");
            
            std::cout <<client;
            std::string cmd = "";
            std::cout << "$ ";
            std::getline(std::cin, cmd);
            if(cmd == "text"){
                std::cout << "text>>";
                auto msg = new TextMessage;
                msg->read(std::cin);
                client.SendMessage(msg);
            } else 
            if(cmd == "position"){
                std::cout << "position>>";
                auto msg = new PositionMessage;
                msg->read(std::cin);
                client.SendMessage(msg);
            } else 
            if(cmd == "image"){
                std::cout << "image>>";
                auto msg = new ImageMessage;
                msg->read(std::cin);
                client.SendMessage(msg);
            }  
            else std::cout <<client;
                   
        }
    }
    catch(std::string e){
        std::cout <<e << std::endl;
    }
    return 0;
}
