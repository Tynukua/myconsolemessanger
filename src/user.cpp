#include "user.h"
#include <fstream>

void User::write(std::ofstream& os){
    size_t t = username.size();
    os.write((char*) &t, sizeof(size_t));
    for (int i = 0;i<username.size();i++){
        os.put(username[i]);
    }
}
void User::write(OSocketFileStream& os){
    size_t t = username.size();
    os.write((char*) &t, sizeof(size_t));
    for (int i = 0;i<username.size();i++){
        os.put(username[i]);
    }
}

void User::read(std::ifstream& is){
    size_t n= 0;
    is.read((char*)&n,sizeof(size_t ));
    username = "";
    for (int i = 0;i<username.size();i++){
        username+=is.get();
    }

}
void User::read(ISocketFileStream& is){
    size_t n= 0;
    is.read((char*)&n,sizeof(size_t ));
    username = "";
    for (int i = 0;i<username.size();i++){
        username+=is.get();
    }

}
std::string User::Username(){
    return username;
}

void User::read(std::ifstream& is, User*&userptr){
    userptr = new User;
    userptr->read(is);
}
void User::read(ISocketFileStream& is, User*&userptr){
    userptr = new User;
    userptr->read(is);
}
