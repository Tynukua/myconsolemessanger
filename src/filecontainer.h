#ifndef FILECONTAINER_H
#define FILECONTAINER_H

#include <fstream>
#include <iostream>
#include "socketfile.h"

template <class T>
class FileContainer
{
    friend class Client;
    T** items = 0;
    size_t len = 0;
    size_t size = 0;
    
    std::string filename;
    void realloc(){
        if(!items){
            items = new T*[size=256]{0};
            len = 0;
        }
        else{
            auto newitems = new T*[size*=2]{0};
            for(size_t i =0;i<len;i++){
                newitems[i] = items[i];
            }
            delete [] items;
            items = newitems;
        }
    }
    void delete_(){
        for (size_t i = 0;i<size;i++){
            if(items[i])
                delete items[i];
        }
        delete [] items;
        items = 0;
    }
public:
    FileContainer(std::string _filename){
        filename= _filename;
    }
    
    void load(std::ifstream&f){
        f.read((char*)&len, sizeof(len));
        delete_();
        items = new T*[size=2*len+1]{0};
        for(size_t i = 0;i<len;i++){
            T::read(f, items[i]);
        }
    
    }    
    void load(ISocketFileStream&f){
        f.read((char*)&len, sizeof(len));
        delete_();
        items = new T*[size=2*len+1]{0};
        for(size_t i = 0;i<len;i++){
            T::read(f, items[i]);
        }
    
    }
    void load(){
        std::ifstream f;
        f.open(filename);
        load(f);
        f.close();
    }

    void save(std::ofstream&f){
        f.write((char*)&len, sizeof(len));
        for(size_t i = 0;i<len;i++){
            if(items[i])
                items[i]->write(f);
        }
    }    
    void save(OSocketFileStream&f){
        f.write((char*)&len, sizeof(len));
        for(size_t i = 0;i<len;i++){
            if(items[i])
                items[i]->write(f);
        }
    }

    void save(){
        std::ofstream f;
        f.open(filename);
        save(f);
        f.flush();
        f.close();
    }
    size_t Len(){return len;}
    T*& operator[](size_t _i){
        if(_i < len) return items[_i];
        throw std::exception();
    }
    void add(T* _i){
        if(len ==size) realloc();
        items[len++] = _i;

    }
    void display(std::ostream& os=std::cout);

};

#endif
