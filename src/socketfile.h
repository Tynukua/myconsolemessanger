#ifndef  SOCKETFILE_H
#define SOCKETFILE_H

#include <fstream>

class OSocketFileStream{
    int socketfd = -1;
    public:
    OSocketFileStream(int);
    void write (const char* s, std::streamsize n);
    void put (char c);
};

class ISocketFileStream{
    int socketfd = -1;
    public:
    ISocketFileStream(int);
    void read (char* s, std::streamsize n);
    int get();
};

#endif
