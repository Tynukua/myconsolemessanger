#include "client.h"
#include "socketfile.h"

bool is_new_time(time_t l, time_t r){
    tm * tml = localtime(&l);
    tm * tmr = localtime(&r);
    if( tml->tm_year!= tmr->tm_year||
        tml->tm_mday!=tmr->tm_mday||
        tml->tm_mon!=tmr->tm_mon){
        std::cout <<"==="<< tmr->tm_mday << '.'<< tmr->tm_mon << '.'<< tmr->tm_year<<"==="<<std::endl;return 1;
    }
    else return 0;
}

Client::Client(char * ipstr, char * name): messages(""), users("") {
    serverip = ipstr;   
    username = name;
}

void Client::connect(){
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
    {   
        throw std::string("ClienSocketCreationFailed");
    }

    serv_addr.sin_family = AF_INET; 
    serv_addr.sin_port = htons(8080); 
    
    if(inet_pton(AF_INET, serverip, &serv_addr.sin_addr)<=0)  
    { 
        throw std::string("AddressFailed");
    } 
    if(::connect(sock,(struct sockaddr *)&serv_addr ,sizeof(serv_addr) )){
        throw std::string("ConnectFailed");
    }
}

void Client::GetChat(){
    RequestTypes t;
    connect();
    OSocketFileStream os(sock);
    t = GetChatRequest;
    os.write((char *) &t, sizeof(t));
    ISocketFileStream is(sock);
    messages.load(is);
    close(sock);
}

void Client::GetMyUser(){
    RequestTypes t = GetMyUserRequest;
    connect();
    OSocketFileStream os(sock);
    os.write((char *)& t, sizeof(t));
    size_t len = username.size();
    os.write((char*) &len,sizeof(len));
    char * buf = new char[len+1]{0};
    for(size_t i = 0;i<len;i++){buf[i] = username[i];}
    os.write(buf, len);
    ISocketFileStream is(sock);
    size_t id;
    is.read((char*) &id, sizeof(id));
    close(sock);
}

std::ostream& operator<<(std::ostream&os, Client& t){
    Message * lastmessage;
    t.GetUsers();
    t.GetChat();
    size_t len = t.messages.Len();
    for (size_t i = 0;i<len;i++){
        if(!i) {
            lastmessage = t.messages[0];
            auto tmptime = lastmessage->Time();
            tm * msgtime = localtime(&tmptime);
            std::cout <<"==="<< msgtime->tm_mday << '.'<< msgtime->tm_mon << '.'<< msgtime->tm_year<<"==="<<std::endl;
            if(lastmessage->UserId()>=t.users.Len())
            {
                std::cout << "UnknownUser\n";
            }
            else{
                std::cout << t.users[lastmessage->UserId()]->Username()<<'\n';
            }
        }else{
            is_new_time(lastmessage->Time(), t.messages[i]->Time());
            if(lastmessage->UserId()!=t.messages[i]->UserId()){
                if(lastmessage->UserId()>=t.users.Len()) {
                    std::cout << "UnknownUser\n";
                }
                else{
                    std::cout << t.users[lastmessage->UserId()]->Username()<<'\n';
                }
            }

        }
        t.messages[i]->display(os);
        lastmessage = t.messages[i];

    }
    return os;

}

void Client::SendMessage(Message* m){
    m->UserId() = user_id;
    RequestTypes t = SendMessageRequest;
    connect();
    OSocketFileStream os(sock);
    os.write((char*)&t, sizeof(t));
    m->write(os);
    close(sock);
}

void Client::GetUsers(){
    RequestTypes t;
    connect();
    OSocketFileStream os(sock);
    t = GetUsersRequests;
    os.write((char *) &t, sizeof(t));
    ISocketFileStream is(sock);
    users.load(is);
    close(sock);
}
    

