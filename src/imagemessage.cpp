#include "message.h"

ImageMessage::ImageMessage(char f):Message(){
    Message::GetMessageType() = ImageMessageType;
    for ( char *i: image){
        for(int j =0;j<20;j++){
            i[j] = f;
        }
    }
}

void ImageMessage::display(std::ostream &os){
    for ( char *i: image){
        for(int j =0;j<20;j++){
            os.put(i[j]);
        }
    }
}

void ImageMessage::read(ISocketFileStream&is){
    Message::read(is);
    for (int i = 0;i<20;i++){
        for(int j = 0;j<20;j++){
            image[i][j] = is.get();
        }
    }
}

void ImageMessage::read(std::istream&is){
    Message::read(is);
    for (int i = 0;i<20;i++){
        for(int j = 0;j<20;j++){
            image[i][j] = is.get();
            if(image[i][j] =='\n') j = 21;
        }
    }
}
void ImageMessage::read(std::ifstream&is){
    Message::read(is);
    for (int i = 0;i<20;i++){
        for(int j = 0;j<20;j++){
            image[i][j] = is.get();
        }
    }
}

void ImageMessage::write(std::ofstream &os){
    Message::write(os);
    for (int i = 0;i<20;i++){
        for(int j = 0;j<20;j++){
            os.put(image[i][j]);
        }
    }
}
void ImageMessage::write(OSocketFileStream &os){
    Message::write(os);
    for (int i = 0;i<20;i++){
        for(int j = 0;j<20;j++){
            os.put(image[i][j]);
        }
    }
}
