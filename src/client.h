
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <unistd.h> 
#include "request.h"
#include "message.h"
#include "user.h"


class Client {
    Users users;
    const char * serverip;
    int sock = 0;
    size_t user_id = 0;
    std::string username = "admin";
    Messages messages;
    sockaddr_in serv_addr;
    void connect();
public:
    friend std::ostream& operator<<(std::ostream&os, Client& t);
    Client(char * servipaddr, char * name);
    void GetChat();
    void SendMessage(Message*);
    void GetMyUser();
    void GetUsers();
};


