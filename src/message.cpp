#include "message.h"
#include <fstream>

MessageTypes& Message::GetMessageType(){
    return msg_type;
}

void Message::read(std::ifstream &is, Message *&msg){
    MessageTypes t;
    is.read((char*)&t, sizeof t);
    switch(t){
        case TextMessageType:
            msg = new TextMessage();
            break;
        case PositionMessageType:
            msg = new PositionMessage();
            break;
        case ImageMessageType:
            msg = new ImageMessage();
            break;
        default:
        case MessageType:
            msg = new Message();
            break;
    }
    msg->read(is);
}

void Message::read(ISocketFileStream &is, Message *&msg){
    MessageTypes t;
    is.read((char*)&t, sizeof t);
    switch(t){
        case TextMessageType:
            msg = new TextMessage();
            break;
        case PositionMessageType:
            msg = new PositionMessage();
            break;
        case ImageMessageType:
            msg = new ImageMessage();
            break;
        default:
        case MessageType:
            msg = new Message();
            break;
    }
    msg->read(is);
}

Message::Message(){
    time(&_time);
}

time_t Message::Time(){return _time;}
size_t& Message::UserId(){return user_id;}

void Message::display(std::ostream &os){
    std::endl(os);
}

void Message::read(std::ifstream&is){
    is.read((char*)&user_id, sizeof(int));
    is.read((char*)&_time, sizeof(_time));
}

void Message::read(std::istream&is){
    time(&_time);
}

void Message::write(std::ofstream &os){
    os.write((char*)&msg_type,sizeof(msg_type));
    os.write((char*)&user_id, sizeof(int));
    os.write((char*)&_time, sizeof(_time));
}

void Message::write(OSocketFileStream &os){
    os.write((char*)&msg_type,sizeof(msg_type));
    os.write((char*)&user_id, sizeof(int));
    os.write((char*)&_time, sizeof(_time));
}

void Message::read(ISocketFileStream&is){
    is.read((char*)&user_id, sizeof(int));
    is.read((char*)&_time, sizeof(_time));
}
